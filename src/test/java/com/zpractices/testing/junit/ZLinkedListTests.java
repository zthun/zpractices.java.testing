/*
 * The zlib/libpng License
 * Copyright (c) 2017 Zthunworks
 *
 * This software is provided 'as-is', without any express 
 * or implied warranty. In no event will the authors be 
 * held liable for any damages arising from the use of 
 * this software.
 * 
 * Permission is granted to anyone to use this software 
 * for any purpose, including commercial applications, 
 * and to alter it and redistribute it freely, subject 
 * to the following restrictions:
 *
 * 1. The origin of this software must not be 
 *    misrepresented; you must not claim that you 
 *    wrote the original software. If you use this 
 *    software in a product, an acknowledgment in the 
 *    product documentation would be appreciated but 
 *    is not required.
 *
 * 2. Altered source versions must be plainly marked as 
 *    such, and must not be misrepresented as being the 
 *    original software.
 *
 * 3. This notice may not be removed or altered from any 
 *    source distribution.
 */
package com.zpractices.testing.junit;

import com.zpractices.testing.ZNode;
import com.zpractices.testing.ZLinkedList;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import org.junit.Before;

/**
 * Represents tests for the ZLinkedList class.
 *
 * @author Anthony Bonta
 */
public final class ZLinkedListTests {

    private Comparator<Integer> mComparer;

    /**
     * Creates the test target.
     *
     * @return The target under test.
     */
    private ZLinkedList<Integer> createTestTarget() {
        return new ZLinkedList<>();
    }

    /**
     * Ran before each test.
     */
    @Before
    public void setup() {
        mComparer = new Comparator<Integer>() {
            public int compare(final Integer a, final Integer b) {
                return a - b;
            };
        };
    }

    /**
     * Test: The size matches the number of nodes in the list.
     */
    @Test
    public void sizeReturnsTheTotalNumberOfNodesInTheList() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(0);
        target.addBack(1);
        target.addBack(2);
        target.removeBack();
        target.addBack(3);
        target.removeBack();
        // Act 
        long size = target.size();
        // Assert
        assertEquals(2, size);
    }

    /**
     * Test: The data is added to the front.
     */
    @Test
    public void addFrontAddsToTheFrontOfTheList() {
        // Arrange 
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        target.addBack(3);
        // Act 
        target.addFront(4);
        // Assert
        assertEquals(new Integer(4), target.front());
    }

    /**
     * Test: The node added is returned.
     */
    @Test
    public void addFrontReturnsTheNodeAdded() {
        // Arrange 
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        // Act 
        ZNode<Integer> front = target.addFront(4);
        // Assert
        assertEquals(new Integer(4), front.getData());
    }

    /**
     * Test: The front node is updated.
     */
    @Test
    public void addFrontUpdatesTheFrontNode() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        // Act
        target.addFront(4);
        // Assert
        assertEquals(new Integer(4), target.frontNode().getData());
    }

    /**
     * Test: The node is added to the back of the list.
     */
    @Test
    public void addBackAddsToTheBackOfTheList() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(3);
        target.addFront(2);
        target.addFront(1);
        // Act 
        target.addBack(4);
        // Assert
        assertEquals(new Integer(4), target.back());
    }

    /**
     * Test: The node added is returned.
     */
    @Test
    public void addBackReturnsTheNodeAdded() {
        // Arrange 
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(3);
        target.addFront(2);
        // Act 
        ZNode<Integer> node = target.addBack(4);
        // Assert
        assertEquals(new Integer(4), node.getData());
    }

    /**
     * Test: The back node is updated.
     */
    @Test
    public void addBackUpdatesTheBackNode() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(3);
        target.addFront(2);
        // Act
        target.addBack(4);
        // Assert
        assertEquals(new Integer(4), target.backNode().getData());
    }

    /**
     * Test: The node is inserted before the specified node.
     */
    @Test
    public void insertBeforeInsertsBeforeTheSpecifiedNode() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        ZNode<Integer> before = target.addBack(2);
        ZNode<Integer> after = target.addBack(4);
        // Act 
        ZNode<Integer> node = target.insertBefore(3, after);
        // Assert 
        assertSame(before, node.getPrevious());
        assertSame(after, node.getNext());
        assertSame(node, after.getPrevious());
        assertSame(node, before.getNext());
    }

    /**
     * Test: The node created is returned.
     */
    @Test
    public void insertBeforeReturnsTheNode() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        ZNode<Integer> insertBefore = target.addBack(3);
        // Act 
        ZNode<Integer> inserted = target.insertBefore(2, insertBefore);
        // Assert
        assertEquals(new Integer(2), inserted.getData());
    }

    /**
     * Test: An exception is thrown.
     */
    @Test(expected = IllegalStateException.class)
    public void insertBeforeThrowsExceptionIfTheNodeIsNotOwned() {
        // Arrange 
        ZLinkedList<Integer> target = createTestTarget();
        ZLinkedList<Integer> other = createTestTarget();
        ZNode<Integer> otherNode = other.addBack(1);
        // Act 
        target.insertBefore(2, otherNode);
        // Assert
        // Handled by test statement.
    }

    /**
     * Test: The front node is overtaken.
     */
    @Test
    public void insertBeforeUpdatesTheFrontNodeIfItOvertakesTheExistingFrontNode() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        ZNode<Integer> front = target.addBack(2);
        target.addBack(3);
        // Act
        ZNode<Integer> inserted = target.insertBefore(1, front);
        // Assert
        assertSame(inserted, target.frontNode());
    }

    /**
     * Test: The front node is updated.
     */
    @Test
    public void insertBeforeInsertsToTheFrontIfTheNodeIsNull() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(2);
        // Act
        target.insertBefore(1, null);
        // Assert
        assertEquals(new Integer(1), target.front());
    }

    /**
     * Test: The size is updated.
     */
    @Test
    public void insertBeforeUpdatesTheSize() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.insertBefore(1, null);
        // Act
        target.insertBefore(2, null);
        // Assert
        assertEquals(2, target.size());

    }

    /**
     * Test: The node is inserted after another.
     */
    @Test
    public void insertAfterInsertsAfterTheSpecifiedNode() {
        // Arrange 
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        ZNode<Integer> before = target.addBack(2);
        ZNode<Integer> after = target.addBack(4);
        // Act
        ZNode<Integer> inserted = target.insertAfter(3, before);
        // Assert 
        assertSame(before, inserted.getPrevious());
        assertSame(after, inserted.getNext());
        assertSame(inserted, before.getNext());
        assertSame(inserted, after.getPrevious());
    }

    /**
     * Test: The node is returned.
     */
    @Test
    public void insertAfterReturnsTheCreatedNode() {
        // Arrange 
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        ZNode<Integer> before = target.addBack(2);
        ZNode<Integer> after = target.addBack(4);
        // Act
        ZNode<Integer> inserted = target.insertAfter(3, before);
        // Assert 
        assertSame(before, inserted.getPrevious());
        assertSame(after, inserted.getNext());
        assertSame(inserted, before.getNext());
        assertSame(inserted, after.getPrevious());
    }

    /**
     * Test: An exception is thrown.
     */
    @Test(expected = IllegalStateException.class)
    public void insertAfterThrowsExceptionIfTheNodeIsNotOwned() {
        // Arrange 
        ZLinkedList<Integer> target = createTestTarget();
        ZLinkedList<Integer> other = createTestTarget();
        ZNode<Integer> otherNode = other.addBack(1);
        // Act 
        target.insertAfter(2, otherNode);
        // Assert
        // Handled by test statement.
    }

    /**
     * Test: The back node is update.
     */
    @Test
    public void insertAfterInsertsAtTheEndIfTheWhereIsNull() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(1);
        // Act
        target.insertAfter(2, null);
        // Assert
        assertEquals(new Integer(2), target.back());
    }

    /**
     * Test: The back node is updated.
     */
    @Test
    public void insertAfterUpdatesTheBackNodeIfItWasReplaced() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        ZNode<Integer> back = target.addBack(2);
        // Act
        ZNode<Integer> inserted = target.insertAfter(2, back);
        // Assert
        assertSame(inserted, target.backNode());
    }

    /**
     * The size is updated.
     */
    @Test
    public void insertAfterUpdatesTheSize() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.insertAfter(1, null);
        // Act
        target.insertAfter(2, null);
        // Assert
        assertEquals(2, target.size());
    }

    /**
     * Test: The back and front nodes are the same with 1 item.
     */
    @Test
    public void frontNodeIsEqualToBackNodeIfOnlyOneNodeIsInTheList() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        // Act
        target.addFront(1);
        // Assert
        assertSame(target.frontNode(), target.backNode());
    }

    /**
     * Test: The data at the front is null if the list is empty.
     */
    @Test
    public void frontIsNullIfListIsEmpty() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(1);
        target.addBack(2);
        // Act
        target.clear();
        // Assert
        assertNull(target.front());
    }

    /**
     * Test: The front node is null.
     */
    @Test
    public void frontNodeIsNullIfListIsEmpty() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(1);
        target.addBack(2);
        // Act
        target.clear();
        // Assert
        assertNull(target.frontNode());
    }

    /**
     * Test: The back data is null.
     */
    @Test
    public void backIsNullIfListIsEmpty() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(1);
        target.addBack(2);
        // Act
        target.clear();
        // Assert
        assertNull(target.back());
    }

    /**
     * Test: The back node is null.
     */
    @Test
    public void backNodeIsNullIfListIsEmpty() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(1);
        target.addBack(2);
        // Act
        target.clear();
        // Assert
        assertNull(target.backNode());
    }

    /**
     * Test: The list is empty after a clear.
     */
    @Test
    public void clearRemovesAllNodes() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        // Act
        target.clear();
        // Assert
        assertEquals(0, target.size());
    }

    /**
     * Test: The list no longer has a front node.
     */
    @Test
    public void clearRemovesTheFrontNode() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        // Act
        target.clear();
        // Assert
        assertNull(target.frontNode());
    }

    /**
     * Test: The list no longer has a back node.
     */
    @Test
    public void clearRemovesTheBackNode() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        // Act
        target.clear();
        // Assert
        assertNull(target.backNode());
    }

    /**
     * The front node is removed.
     */
    @Test
    public void removeFrontReturnsTheNodeRemoved() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addFront(2);
        ZNode<Integer> expected = target.addFront(1);
        // Act
        ZNode<Integer> removed = target.removeFront();
        // Assert
        assertSame(expected, removed);
    }

    /**
     * Null is returned.
     */
    @Test
    public void removeFrontReturnsNullIfTheCollectionIsEmpty() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        // Act
        ZNode<Integer> removed = target.removeFront();
        // Assert
        assertNull(removed);
    }

    /**
     * The back node is removed.
     */
    @Test
    public void removeBackReturnsTheNodeRemoved() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        ZNode<Integer> expected = target.addBack(2);
        // Act
        ZNode<Integer> removed = target.removeBack();
        // Assert
        assertSame(expected, removed);
    }

    /**
     * Null is returned.
     */
    @Test
    public void removeBackReturnsNullIfTheCollectionIsEmpty() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        // Act
        ZNode<Integer> removed = target.removeBack();
        // Assert
        assertNull(removed);
    }

    /**
     * Test: The data is removed.
     */
    @Test
    public void removeRemovesTheData() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        target.addBack(3);
        target.addBack(4);
        // Act
        target.remove(3, mComparer);
        // Assert
        assertNull(target.find(3, mComparer));
    }
    
    /**
     * Null is returned.
     */
    @Test
    public void removeDataReturnsNullIfNoDataWasFound() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        target.addBack(4);
        // Act
        ZNode<Integer> removed = target.remove(3, mComparer);
        // Assert
        assertNull(removed);
    }

    /**
     * Test: The node is removed.
     */
    @Test
    public void removeRemovesTheNode() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        ZNode<Integer> removeThis = target.addBack(3);
        target.addBack(4);
        // Act
        target.remove(removeThis);
        // Assert
        assertNull(target.find(3, mComparer));
    }

    /**
     * Test: The size is decremented.
     */
    @Test
    public void removeDecrementsTheSize() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        target.addBack(3);
        // Act
        target.remove(3, mComparer);
        // Assert
        assertEquals(2, target.size());
    }

    /**
     * Test: An exception is thrown.
     */
    @Test(expected = NullPointerException.class)
    public void removeThrowsExceptionIfNodeIsNull() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        // Act
        target.remove(null);
        // Assert
        // Handled by test statement.
    }

    /**
     * Test: An exception is thrown.
     */
    @Test(expected = IllegalStateException.class)
    public void removeThrowsExceptionIfTheNodeDoesNotBelongToTheList() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        ZLinkedList<Integer> other = createTestTarget();
        ZNode<Integer> otherData = other.addFront(2);
        // Act
        target.remove(otherData);
        // Assert
        // Handled by test statement.
    }

    /**
     * Test: The data is found.
     */
    @Test
    public void findReturnsTheData() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        target.addBack(3);
        target.addBack(4);
        // Act
        ZNode<Integer> found = target.find(3, mComparer);
        // Assert
        assertEquals(new Integer(3), found.getData());
    }

    /**
     * Test: Null is returned.
     */
    @Test
    public void findReturnsNullIfNoDataWasFound() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        target.addBack(4);
        // Act
        ZNode<Integer> found = target.find(3, mComparer);
        // Assert
        assertNull(found);
    }

    /**
     * Null is returned.
     */
    @Test
    public void findReturnsNullOnAnEmptyList() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        // Act
        ZNode<Integer> found = target.find(3, mComparer);
        // Assert
        assertNull(found);
    }

    /**
     * The iterator moves through the entire collection.
     */
    @Test
    public void iteratorReturnsAnObjectThatIteratesThroughTheEntireCollection() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        target.addBack(3);
        target.addBack(4);
        Integer[] expected = new Integer[]{1, 2, 3, 4};
        ArrayList<Integer> actual = new ArrayList<>();
        // Act
        for (Integer i : target) {
            actual.add(i);
        }
        // Assert
        assertEquals(expected.length, actual.size());
        for (int i = 0; i < expected.length; ++i) {
            assertEquals(expected[i], actual.get(i));
        }
    }

    /**
     * Remove is not allowed.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void iteratorRemoveIsNotAllowed() {
        // Arrange
        ZLinkedList<Integer> target = createTestTarget();
        target.addBack(1);
        target.addBack(2);
        target.addBack(3);
        Iterator<Integer> itr = target.iterator();
        itr.next();
        // Act
        itr.remove();
        // Assert
        // Handled by test statement.
    }
}
