/*
 * The zlib/libpng License
 * Copyright (c) 2017 Zthunworks
 *
 * This software is provided 'as-is', without any express 
 * or implied warranty. In no event will the authors be 
 * held liable for any damages arising from the use of 
 * this software.
 * 
 * Permission is granted to anyone to use this software 
 * for any purpose, including commercial applications, 
 * and to alter it and redistribute it freely, subject 
 * to the following restrictions:
 *
 * 1. The origin of this software must not be 
 *    misrepresented; you must not claim that you 
 *    wrote the original software. If you use this 
 *    software in a product, an acknowledgment in the 
 *    product documentation would be appreciated but 
 *    is not required.
 *
 * 2. Altered source versions must be plainly marked as 
 *    such, and must not be misrepresented as being the 
 *    original software.
 *
 * 3. This notice may not be removed or altered from any 
 *    source distribution.
 */
package com.zpractices.testing.junit;

import com.zpractices.testing.ZNode;
import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

/**
 * Represents tests for the ZNode class.
 *
 * @author Anthony Bonta
 */
public final class ZNodeTests {

    private Object mOwner;

    /**
     * Ran before every test.
     */
    @Before
    public void setup() {
        mOwner = new Object();
    }

    /**
     * Test: The owner is set in the constructor.
     */
    @Test
    public void constructorSetsTheOwner() {
        // Arrange & Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner);
        // Assert 
        assertSame(mOwner, testTarget.getOwner());
    }

    /**
     * Test: When nothing is passed to the constructor, the data should be null.
     */
    @Test
    public void constructorSetsDataToNullWithNoArguments() {
        // Arrange & Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner);
        // Assert
        assertNull(testTarget.getData());
    }

    /**
     * Test: When nothing is passed to the constructor, the next node should be
     * null.
     */
    @Test
    public void constructorSetsNextToNullWithNoArguments() {
        // Arrange & Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner);
        // Assert
        assertNull(testTarget.getNext());
    }

    /**
     * Test: When nothing is passed to the constructor, the prev node should be
     * null.
     */
    @Test
    public void constructorSetsPrevToNullWithNoArguments() {
        // Arrange & Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner);
        // Assert
        assertNull(testTarget.getPrevious());
    }

    /**
     * Test: When only the data is passed, it should set the data.
     */
    @Test
    public void constructorSetsDataWhenOnlyDataIsPassed() {
        // Arrange
        Integer data = 550;
        // Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner, data);
        // Assert
        assertEquals(data, testTarget.getData());
    }

    /**
     * Test: When on the data is passed, the next pointer should be null.
     */
    @Test
    public void constructorSetsNextToNullWhenOnlyDataIsPassed() {
        // Arrange & Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner, 50);
        // Assert
        assertNull(testTarget.getNext());
    }

    /**
     * Test: When only the data is passed, the previous pointer should be null.
     */
    @Test
    public void constructorSetsPrevToNullWhenOnlyDataIsPassed() {
        // Arrange & Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner, 50);
        // Assert
        assertNull(testTarget.getPrevious());
    }

    /**
     * Test: When all values are passed, the data should be set.
     */
    @Test
    public void constructorSetsDataWhenAllParametersArePassed() {
        // Arrange 
        Integer data = 550;
        // Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner, data, null, null);
        // Assert
        assertEquals(data, testTarget.getData());
    }

    /**
     * Test: When all values are passed, the next pointer should be set.
     */
    @Test
    public void constructorSetsNextWhenAllParametersArePassed() {
        // Arrange 
        Integer data = 550;
        ZNode<Integer> next = new ZNode<>(600);
        // Act 
        ZNode<Integer> testTarget = new ZNode<>(mOwner, data, next, null);
        // Assert
        assertSame(next, testTarget.getNext());
    }

    /**
     * Test: When all values are passed, the previous pointer should be set.
     */
    @Test
    public void constructorSetsPrevWhenAllParametersArePassed() {
        // Arrange 
        Integer data = 550;
        ZNode<Integer> prev = new ZNode<>(400);
        // Act
        ZNode<Integer> testTarget = new ZNode<>(mOwner, data, null, prev);
        // Assert
        assertSame(prev, testTarget.getPrevious());
    }

    /**
     * Test: The previous node should be set.
     */
    @Test
    public void setPreviousShouldSetThePreviousNode() {
        // Arrange
        ZNode<Integer> prev = new ZNode<>(200);
        ZNode<Integer> testTarget = new ZNode<>(mOwner, 300);
        // Act 
        testTarget.setPrevious(prev);
        // Assert 
        assertSame(prev, testTarget.getPrevious());
    }

    /**
     * Test: The node that was set as the previous before the updated node is
     * returned.
     */
    @Test
    public void setPreviousReturnsThePreviouslySetNode() {
        // Arrange 
        ZNode<Integer> prev = new ZNode<>(mOwner, 200);
        ZNode<Integer> oldPrev = new ZNode<>(mOwner, 199);
        ZNode<Integer> testTarget = new ZNode<>(mOwner, 300, null, oldPrev);
        // Act 
        ZNode<Integer> shouldBeOld = testTarget.setPrevious(prev);
        // Assert 
        assertSame(oldPrev, shouldBeOld);
    }

    /**
     * Test: The node that gets set is the value returned.
     */
    @Test
    public void setNextSetsTheNextNode() {
        // Arrange
        ZNode<Integer> next = new ZNode<>(mOwner, 350);
        ZNode<Integer> testTarget = new ZNode<>(mOwner, 300);
        // Act 
        testTarget.setNext(next);
        // Assert 
        assertSame(next, testTarget.getNext());
    }

    /**
     * Test: The old node that was replaced is the node returned.
     */
    @Test
    public void setNextReturnsThePreviousNextNode() {
        // Arrange
        ZNode<Integer> next = new ZNode<>(mOwner, 350);
        ZNode<Integer> oldNext = new ZNode<>(mOwner, 349);
        ZNode<Integer> testTarget = new ZNode<>(mOwner, 300, oldNext, null);
        // Act 
        ZNode<Integer> shouldBeOld = testTarget.setNext(next);
        // Assert 
        assertSame(oldNext, shouldBeOld);
    }

    /**
     * Test: The data gets set.
     */
    @Test
    public void setDataSetsTheDataForTheNode() {
        // Arrange 
        Integer data = 675;
        ZNode<Integer> testTarget = new ZNode<>(mOwner);
        // Act 
        testTarget.setData(data);
        // Assert
        assertEquals(data, testTarget.getData());
    }

    /**
     * Test: The old data is returned.
     */
    @Test
    public void setDataReturnsTheOldData() {
        // Arrange 
        Integer data = 675;
        Integer oldData = 674;
        ZNode<Integer> testTarget = new ZNode<>(mOwner, oldData);
        // Act 
        Integer shouldBeOld = testTarget.setData(data);
        // Assert 
        assertEquals(oldData, shouldBeOld);
    }
}
