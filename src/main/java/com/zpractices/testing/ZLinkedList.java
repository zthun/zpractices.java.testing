/*
 * The zlib/libpng License
 * Copyright (c) 2017 Zthunworks
 *
 * This software is provided 'as-is', without any express 
 * or implied warranty. In no event will the authors be 
 * held liable for any damages arising from the use of 
 * this software.
 * 
 * Permission is granted to anyone to use this software 
 * for any purpose, including commercial applications, 
 * and to alter it and redistribute it freely, subject 
 * to the following restrictions:
 *
 * 1. The origin of this software must not be 
 *    misrepresented; you must not claim that you 
 *    wrote the original software. If you use this 
 *    software in a product, an acknowledgment in the 
 *    product documentation would be appreciated but 
 *    is not required.
 *
 * 2. Altered source versions must be plainly marked as 
 *    such, and must not be misrepresented as being the 
 *    original software.
 *
 * 3. This notice may not be removed or altered from any 
 *    source distribution.
 */
package com.zpractices.testing;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Represents an implementation of a linked list.
 *
 * @author Anthony Bonta
 *
 * @param <T> The type of data in the linked list.
 */
public final class ZLinkedList<T> implements Iterable<T> {

    /**
     * Initializes a new instance of this object.
     */
    public ZLinkedList() {
        mFrontNode = new ZNode<>(this);
        mBackNode = new ZNode<>(this, null, null, mFrontNode);
        mFrontNode.setNext(mBackNode);
    }

    private long mSize;

    /**
     * Returns the size of the list.
     *
     * @return The total number of nodes in the linked list.
     */
    public long size() {
        return mSize;
    }

    /**
     * Validates to make sure that node belongs to this linked list.
     *
     * @param node The node to validate.
     */
    private void validateNode(final ZNode<T> node) {
        if (node != null && node.getOwner() != this) {
            throw new IllegalStateException("Where does not belong to this linked list.");
        }
    }

    /**
     * Adds a node to the front of the list.
     *
     * @param data The data to add.
     *
     * @return The node that was added.
     */
    public ZNode<T> addFront(final T data) {
        return insertBefore(data, null);
    }

    /**
     * Adds a node to the back of the list.
     *
     * @param data The data to add.
     *
     * @return The node that was added.
     */
    public ZNode<T> addBack(final T data) {
        return insertAfter(data, null);
    }

    /**
     * Inserts a node before the specified location.
     *
     * @param data The data to insert.
     * @param where The node to insert before. If this is null, then the data is
     * inserted at the beginning of the list.
     *
     * @return The node that was added.
     */
    public ZNode<T> insertBefore(final T data, final ZNode<T> where) {
        validateNode(where);

        ZNode<T> nodeToInsert = where == null ? mFrontNode.getNext() : where;
        ZNode<T> dataNode = new ZNode<>(this, data, nodeToInsert, nodeToInsert.getPrevious());
        nodeToInsert.getPrevious().setNext(dataNode);
        nodeToInsert.setPrevious(dataNode);

        mSize += 1;
        return dataNode;
    }

    /**
     * Inserts a node after the specified location.
     *
     * @param data The data to insert.
     * @param where The node to insert after. If this is null, then the data is
     * inserted at the end of the list.
     *
     * @return The node that was added.
     */
    public ZNode<T> insertAfter(final T data, final ZNode<T> where) {
        validateNode(where);
        ZNode<T> nodeToInsert = where == null ? mBackNode : where.getNext();
        return insertBefore(data, nodeToInsert);
    }

    /**
     * Removes the node at the front of the list.
     *
     * @return The node that was removed.
     */
    public ZNode<T> removeFront() {
        if (size() == 0) {
            return null;
        }

        ZNode<T> nodeToRemove = mFrontNode.getNext();
        remove(nodeToRemove);
        return nodeToRemove;
    }

    /**
     * Removes the node at the back of the list.
     *
     * @return The node that was removed.
     */
    public ZNode<T> removeBack() {
        if (size() == 0) {
            return null;
        }

        ZNode<T> nodeToRemove = mBackNode.getPrevious();
        remove(nodeToRemove);
        return nodeToRemove;
    }

    /**
     * Removes data from the list.
     *
     * @param data The data to remove.
     * @param comparer The comparer object used to determine the data to remove.
     *
     * @return The node that was removed.
     */
    public ZNode<T> remove(final T data, final Comparator<T> comparer) {
        ZNode<T> nodeToRemove = find(data, comparer);

        if (nodeToRemove != null) {
            remove(nodeToRemove);
        }

        return nodeToRemove;
    }

    /**
     * Removes a node from the list.
     *
     * @param node The node to remove.
     */
    public void remove(final ZNode<T> node) {
        validateNode(node);

        if (node == null) {
            throw new NullPointerException("node cannot be null.");
        }

        node.getPrevious().setNext(node.getNext());
        node.getNext().setPrevious(node.getPrevious());
        node.setPrevious(null);
        node.setNext(null);

        mSize -= 1;
    }

    /**
     * Removes all nodes from the list.
     */
    public void clear() {
        mFrontNode.setNext(mBackNode);
        mBackNode.setPrevious(mFrontNode);
        mSize = 0;
    }

    /**
     * Searches for a node in the list.
     *
     * @param data The data to search for.
     * @param comparer The comparer that can be used to find the data.
     *
     * @return The node that was found or null if no such data exists in the
     * list.
     */
    public ZNode<T> find(final T data, final Comparator<T> comparer) {
        for (ZNode<T> current = mFrontNode.getNext(); current != mBackNode; current = current.getNext()) {
            if (comparer.compare(data, current.getData()) == 0) {
                return current;
            }
        }
        return null;
    }

    /**
     * Gets the data at the front of the list.
     *
     * @return The data at the front of the list. Returns null if the list is
     * empty.
     */
    public T front() {
        // If the list is empty, then the next node is the back one and it 
        // has null data.
        return mFrontNode.getNext().getData();
    }

    private final ZNode<T> mFrontNode;

    /**
     * Gets the node in the front.
     *
     * @return The front node.
     */
    public ZNode<T> frontNode() {
        ZNode<T> nextNode = mFrontNode.getNext();
        return nextNode == mBackNode ? null : nextNode;
    }

    /**
     * Gets the data at the back of the list.
     *
     * @return The data at the back of the list.
     */
    public T back() {
        // If the list is empty, then the back node's previous points to 
        // the front node stub that has null data.
        return mBackNode.getPrevious().getData();
    }

    private final ZNode<T> mBackNode;

    /**
     * Gets the data at the back of the list.
     *
     * @return The data at the back of the list. Returns null if the list is
     * empty.
     */
    public ZNode<T> backNode() {
        ZNode<T> prevNode = mBackNode.getPrevious();
        return prevNode == mFrontNode ? null : prevNode;
    }

    /**
     * Gets an object that can be used to iterate over this linked list.
     *
     * @return An iterator that can be used to iterate over this linked list.
     */
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private ZNode<T> mStart = mFrontNode.getNext();

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
            
            @Override
            public boolean hasNext() {
                return mStart != mBackNode;
            }

            @Override
            public T next() {
                T data = mStart.getData();
                mStart = mStart.getNext();
                return data;
            }
        };
    }
}
