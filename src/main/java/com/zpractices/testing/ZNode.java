/*
 * The zlib/libpng License
 * Copyright (c) 2017 Zthunworks
 *
 * This software is provided 'as-is', without any express 
 * or implied warranty. In no event will the authors be 
 * held liable for any damages arising from the use of 
 * this software.
 * 
 * Permission is granted to anyone to use this software 
 * for any purpose, including commercial applications, 
 * and to alter it and redistribute it freely, subject 
 * to the following restrictions:
 *
 * 1. The origin of this software must not be 
 *    misrepresented; you must not claim that you 
 *    wrote the original software. If you use this 
 *    software in a product, an acknowledgment in the 
 *    product documentation would be appreciated but 
 *    is not required.
 *
 * 2. Altered source versions must be plainly marked as 
 *    such, and must not be misrepresented as being the 
 *    original software.
 *
 * 3. This notice may not be removed or altered from any 
 *    source distribution.
 */
package com.zpractices.testing;

/**
 * Represents a node in a list.
 *
 * @param <T> The type of data that the node represents.
 */
public final class ZNode<T> {

    /**
     * Initializes a new instance of this object.
     *
     * @param owner The object that owns this node.
     */
    public ZNode(final Object owner) {
        this(owner, null, null, null);
    }

    /**
     * Initializes a new instance of this object.
     *
     * @param owner The object that owns this node.
     * @param data The data for the node.
     */
    public ZNode(final Object owner, final T data) {
        this(owner, data, null, null);
    }

    /**
     * Initializes a new instance of this object.
     *
     * @param owner The object that owns this node.
     * @param data The data for the node.
     * @param next The next node.
     * @param prev The previous node.
     */
    public ZNode(final Object owner, final T data, final ZNode<T> next, final ZNode<T> prev) {
        mOwner = owner;
        mData = data;
        mNext = next;
        mPrev = prev;
    }

    private Object mOwner;

    /**
     * Gets the object that owns this node.
     *
     * @return The object that owns the node.
     */
    public Object getOwner() {
        return mOwner;
    }

    private ZNode<T> mPrev;

    /**
     * Gets the previous node in the list.
     *
     * If this is the head, then this should be null.
     *
     * @return The previous node.
     */
    public ZNode<T> getPrevious() {
        return mPrev;
    }

    /**
     * Sets the previous node.
     *
     * This only sets the value. This does not manipulate the existing previous
     * node.
     *
     * @param prev The node to set.
     *
     * @return The previous node that was set before the new new is set.
     */
    public ZNode<T> setPrevious(final ZNode<T> prev) {
        ZNode<T> oldPrev = mPrev;
        mPrev = prev;
        return oldPrev;
    }

    private ZNode<T> mNext;

    /**
     * Gets the next node.
     *
     * @return The next node.
     */
    public ZNode<T> getNext() {
        return mNext;
    }

    /**
     * Sets the next node.
     *
     * This only sets the value. This does not manipulate the existing next
     * node.
     *
     * @param next The node to set.
     *
     * @return The node that was set as the next node before the new node is
     * set.
     */
    public ZNode<T> setNext(final ZNode<T> next) {
        ZNode<T> oldNext = mNext;
        mNext = next;
        return oldNext;
    }

    private T mData;

    /**
     * Gets the data that this node represents.
     *
     * @return The data represented by the node.
     */
    public T getData() {
        return mData;
    }

    /**
     * Sets the data that the node represents.
     *
     * @param data The data to set.
     *
     * @return The data that this node represented before it was set.
     */
    public T setData(final T data) {
        T oldData = mData;
        mData = data;
        return oldData;
    }
}
